import os
import argparse
import multiprocessing as mp


import src.data.preprocessing.nuscenes as nuscenes
import src.data.preprocessing.av2 as av2
import src.data.preprocessing.argoverse as argoverse
from src.utils.preprocessing_helper import count_files, create_data, split_for_mulitprocessing
from src.utils.setup_helper import load_config

########################################################################################################################

# load config
CONFIG = load_config(os.path.dirname(os.path.abspath(__file__)))

########################################################################################################################

if __name__ == '__main__':
    
    # Define the parser
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset', type=str, default=None)

    parser.add_argument('--num_scenarios_train', type=int, default=CONFIG['preprocessing']['process_num_scenarios_train'])
    parser.add_argument('--num_scenarios_val', type=int, default=CONFIG['preprocessing']['process_num_scenarios_val'])
    parser.add_argument('--scenarios_per_file', type=int, default=CONFIG['preprocessing']['scenarios_per_file'])
    args = parser.parse_args()

    if not args.dataset: raise Exception('Dataset must be defined with --dataset')

    # define directory
    data_dir = CONFIG['directory']['path']+args.dataset+'/'+'processed_data/'
    data_dir_raw = CONFIG['directory']['path'] + args.dataset + '/' + CONFIG['directory']['raw']

    #####################################################################################################################

    # get number of cpu cores (-1 as progressbar requires one process)
    num_processes = mp.cpu_count()-1

    #####################################################################################################################

    # create data for both datasets
    for dataset in ['train/', 'val/']:

        # create folder if they dont exist
        if not os.path.exists(data_dir):                           os.mkdir(data_dir)
        if not os.path.exists(data_dir+dataset):                   os.mkdir(data_dir+dataset)

        ##################################################################################################################
        
        print(f'Collecting scenes for {dataset[:-1]} data ...')

        # Argoverse 2 dataset
        if args.dataset == 'av2':
            scenes = av2.get_scenes(data_dir_raw, dataset)
            polyline_class = av2.get_polyline_class(CONFIG['dataset_meta_data']['av2'])
        
        # Argoverse 1 dataset
        elif args.dataset == 'argoverse':
            scenes =  argoverse.get_scenes(data_dir_raw+dataset)
            polyline_class = argoverse.get_polyline_class(data_dir_raw+dataset, CONFIG['dataset_meta_data']['argoverse'])

        # NuScenes dataset
        elif args.dataset == 'nuscenes':
            scenes =  nuscenes.get_scenes(data_dir_raw, dataset)
            polyline_class = nuscenes.get_polyline_class(data_dir_raw, CONFIG['dataset_meta_data']['nuscenes'])

        # Dataset not implemented
        else: raise NotImplementedError

        ##################################################################################################################

        # splilt scenes so they can be processed by multiple processes
        scenes_split, scenario_step, num_files = split_for_mulitprocessing(scenes, 
                                                                           dataset, 
                                                                           num_processes, 
                                                                           args.num_scenarios_train, 
                                                                           args.num_scenarios_val, 
                                                                           args.scenarios_per_file)

        ##################################################################################################################

        print('Initialising processes that create polylines ...')
        processes = []

        # create progressbar process with the count_files function
        p = mp.Process(target=count_files, 
                       args=(   data_dir+dataset, 
                                num_files   ))
        p.start()
        processes.append(p)

        # create processes that create the data
        for i in range(num_processes):
            # processes cant be created if too many processes for num_scenarios (happens if small datasize is processed) -> just pass in this case
            try:
                # create a new process instance and process data using creaet_data
                p =  mp.Process(target=create_data, 
                                args=(  polyline_class, 
                                        scenes_split[i],
                                        data_dir+dataset,
                                        scenario_step*i,
                                        args.scenarios_per_file ))
                p.start()
                processes.append(p)
            except: pass

        # join processes so code only continues after all processes have been executed
        for process in processes:
            process.join()

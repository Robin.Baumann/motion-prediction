import torch
import numpy as np
import os
import os.path as osp
from collections import Counter
from torch.nn.utils.rnn import pad_sequence
from torch.utils.data import Dataset

########################################################################################################################

# Dataset for preprocessed polyline data
class PolylineDataset(Dataset):
    def __init__(self, directory, model, input_len, pred_len, length_percent=None):

        # get model name 
        self.model = model

        # get input/output timesteps
        self.input_len = input_len
        self.pred_len = pred_len

        # length of data used in percent
        self.length_percent = length_percent

        # get data filenames
        self.processed_dir = directory
        self.processed_paths = []
        self.get_file_names()

        # if no data available -> create data first
        if len(self.processed_paths) == 0:
            raise Exception("No data available!")

        # remember last file and data to avoid reloading
        self.last_file_idx = 0
        self.data = np.load(self.processed_paths[0], allow_pickle=True)['arr_0']

        # get len of dataset and file
        self.file_size = len(self.data)
        self.length = len(self.processed_paths)*self.file_size

    def get_file_names(self):

        # get all files in data directory
        for file_name in os.listdir(self.processed_dir):
            if 'data' in file_name:
                self.processed_paths.append(osp.join(self.processed_dir, file_name))

        # shuffle paths but safe sorted array
        self.sorted_paths = self.processed_paths.copy()
        np.random.shuffle(self.processed_paths)

        # only use small part of data if passed
        if self.length_percent:
            last_value = int(np.ceil(len(self.processed_paths) / 100 * self.length_percent))
            self.processed_paths = self.processed_paths[:last_value]

    def get_raw(self, idx):
        # this function is used mainly if data has to be plotted

        # get file and index of indexed scene and return raw data
        data_idx = idx%self.file_size
        file_idx = idx//self.file_size
        raw_data = np.load(self.sorted_paths[file_idx], allow_pickle=True)['arr_0'][data_idx]

        return raw_data[0], raw_data[1], raw_data[2]
    
    def trunc_agent_horizon(self, features):
        return features[features[:, -2] <= self.input_len-1] # featur -2 is timestep
    
    def trunc_pred_horizon(self, gt):
        return gt[:self.pred_len]

    def get_x_graph(self, features):

        # get polyline data without id and cluster (list of polyline ids)
        x = np.array(features[:, :-1])
        cluster = np.array(features[:, -1])
            
        # return np array of input data
        return torch.tensor(x).float(), torch.tensor(cluster).long()

    def get_x_agents(self, features):

        # pad and expand data to shape [num_agents, seq_len, features]
        x = features
        x = np.take_along_axis(x, np.expand_dims(np.argsort(x[:,-1]), -1).repeat(x.shape[-1], 1), axis=0)                       # sort according to polylines
        x = np.split(x, np.cumsum(list(Counter(x[:,-1].tolist()).values()))[:-1])                                               # split after each polyline (discard last empty split)
        x = np.array([np.take_along_axis(p[:,:5], np.expand_dims(np.argsort(p[:,-2]), -1).repeat(5, 1), axis=0) for p in x])    # sort along timestep within each polyline (discard timestep and polyline feature)
    
        # return float tensor
        return  torch.tensor(x).float()
    
    def get_y_diff(self, gt):

        # calculate the change for every timestep and return as float tensor
        gt_offset = np.vstack([gt[:, 2] - gt[:, 0],  gt[:, 3] - gt[:, 1]]).reshape(-1)
        return torch.tensor(gt_offset).float()

    def __getitem__(self, idx):
        
        # get file and index of indexed scene
        data_idx = idx%self.file_size
        file_idx = idx//self.file_size

        # get scene from correct file if new file is indexed
        if self.last_file_idx != file_idx:
            self.last_file_idx = file_idx
            self.data = np.load(self.processed_paths[file_idx], allow_pickle=True)['arr_0']

        # load the scene and get data
        scene = self.data[data_idx]
        agent_data = self.trunc_agent_horizon(scene[0])
        map_data = scene[1]
        agent_map_data = np.concatenate((agent_data, map_data), axis=0)
        gt = self.trunc_pred_horizon(scene[2])

        # get data according to model
        if self.model == 'VectorNet':   
            x, cluster = self.get_x_graph(agent_map_data)
            y = self.get_y_diff(gt)
            data = {'x': x, 'cluster': cluster, 'y': y}

        elif self.model == 'SimpleLSTM':       
            x = self.get_x_agents(agent_data)[:, :, :-1] # discard semantic information
            y = self.get_y_diff(gt)
            data = {'x': x, 'y': y}

        elif self.model == 'SimpleTransformer':       
            x = self.get_x_agents(agent_data)[:, :, :-1] # discard semantic information
            y = self.get_y_diff(gt)
            data = {'x': x, 'y': y}

        else: raise NotImplementedError

        # return list of data
        return data

    def __len__(self):
        return self.length

########################################################################################################################

# Sampler that only shuffles batches -> used to avoid constant loading of files when accessing dataset randomly
class ShuffleBatchSampler():
    def __init__(self, data_indices, batch_size):

        self.data_indices = data_indices
        self.batch_size = batch_size
        self.len = len(data_indices)

    def __iter__(self):
        # split data into arrays of batch_size
        batch_array = np.array_split(self.data_indices, len(self.data_indices) // self.batch_size)
        # update random seed so new epoch is shuffled different and shuffle batches
        np.random.seed(np.random.get_state()[1][0] + 1)
        np.random.shuffle(batch_array)
        # concat data again to 1D idx array
        output_indices = np.concatenate(batch_array)
        return iter(output_indices)

    def __len__(self):
        return self.len


########################################################################################################################

# collate function that pads data in batch
def collate_fn(batch):

    # init dict
    collated_batch = {}
    
    # Collect all keys present in the samples
    keys = set().union(*[item.keys() for item in batch])
    
    # Iterate over each key and collate the corresponding values
    for key in keys:

        # pad batch and move complete input to device
        values = [item[key] for item in batch]
        if key == 'cluster':    collated_batch[key] = pad_sequence(values, batch_first=True, padding_value=torch.max(torch.cat(values))) # cluster cant be padded with 0 as 0 reveres to focal agent
        else:                   collated_batch[key] = pad_sequence(values, batch_first=True)
    
    return collated_batch
import torch

# calc the l2 distance of two cordinate vectors of shape [[x1, y1,], [x2, y2], ...]
def calc_l2(v1, v2):
    return torch.linalg.norm(v1 - v2, axis=1)

# get average displacemnt error
def get_ade(displacements):
    return displacements.mean()

# get the fde
def get_fde(displacements):
    return displacements[-1].squeeze()

# get miss
def get_miss(displacements, threshold):
    if displacements[-1] > threshold:    return 1
    else:                                   return 0

# get index of last safe timestep meaning timestep where displacement was within a given treshold distance
def get_last_safe_idx(displacements, threshold):
    safe_idx_list = (displacements < threshold).nonzero()   # get list of all timesteps where displacement is below threshold
    if safe_idx_list.shape[0] == 0:
        return 0                                        # no safe displacement for predicted trajectory
    else:                      
        # Todo: remove break (was just a quick solution)
        for i, idx, in enumerate(torch.arange(torch.max(safe_idx_list.squeeze())+1)):
            if safe_idx_list[i] != idx: break
        return i
        
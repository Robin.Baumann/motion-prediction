import torch
from tqdm import tqdm
import src.evaluation.metrics as metrics
import src.utils.coordinate_helper as helper

########################################################################################################################

# loop through data and evaluate metrics
def evaluater(model, data_loader, device, desc="evaluating model"):

    # inint running metrics
    running_loss= 0.0
    running_ade = 0.0
    running_fde = 0.0
    running_miss_rate = 0.0

    # loop through batches
    model.eval()
    with torch.no_grad():
        for i, batch in enumerate(tqdm(data_loader, desc=desc)):

            # move to device
            batch = {k: v.to(device) for k, v in batch.items()}

            # pass data through model
            pred, loss = model.predict(batch)

            # calc metrics for current batch
            batch_loss = loss.item()
            batch_ade, batch_fde, batch_miss_rate = eval_batch(pred, batch['y'])

            # add to running total
            running_loss += batch_loss
            running_ade += batch_ade
            running_fde += batch_fde
            running_miss_rate += batch_miss_rate
    
    # calc averages
    loss = running_loss/(i+1)
    ade = running_ade/(i+1)
    fde = running_fde/(i+1)
    miss_rate = running_miss_rate/(i+1)

    # return mean values for metrics
    return loss, ade, fde, miss_rate

########################################################################################################################

# evaluate metrics for a given batch
def eval_batch(out, gt):

    # init running metrics
    running_ade_batch = 0.0
    running_fde_batch = 0.0
    total_missed = 0

    # calc metrics for every scene
    for i, scene_idx in enumerate(range(gt.size(0))):

        # get coordinates
        x = helper.format_to_coordinates(out[scene_idx])
        y = helper.format_to_coordinates(gt[scene_idx])
        
        # calc metrics based on displacement
        displacements = metrics.calc_l2(x, y)
        running_ade_batch += metrics.get_ade(displacements)
        running_fde_batch += metrics.get_fde(displacements)
        total_missed +=  metrics.get_miss(displacements, threshold=2.0)

    # calc average (check if no scene for condition was found in batch)
    ade_batch = running_ade_batch/(i+1)
    fde_batch = running_fde_batch/(i+1)
    missed_rate_batch = total_missed/(i+1)

    # return mean values for metrics
    return ade_batch, fde_batch, missed_rate_batch

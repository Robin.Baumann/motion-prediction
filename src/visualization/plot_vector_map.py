import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import os
import os.path as osp

from src.data.dataset import PolylineDataset


def plot_vector_map(scene_idx, data_dir, show=True):

    # fov limit
    limit_fov = 30

    # get Polylines
    Polylines = PolylineDataset(directory=data_dir, model=None)

    # plot get random samples
    if not scene_idx:  

        # get all file names in data directory
        paths = []
        for file_name in os.listdir(data_dir):
            if 'data' in file_name: 
                paths.append(osp.join(data_dir, file_name))
        
        # get file size
        file_size = len(np.load(paths[0], allow_pickle=True)['arr_0'])

        # get 9 random scenes
        scenes = np.random.choice(np.arange(len(paths)*file_size), size=9, replace=False)
        

    else:
        scenes = [scene_idx]

    # plot all scenes
    print('Plotting ...')
    figure = plt.figure()
    for i, scene in enumerate(scenes):

        # get data
        p_traj, p_lane, future_traj = Polylines.get_raw(scene)

        # make subplot if samples should be plotted
        if not scene_idx:
            plt.subplot(3, 3, i+1)
            plt.title(f'Scene:{scene}')

        # create plot
        for v in p_lane:
            if v[0] == v[1] == v[2] == v[3] == 0: continue
            plt.quiver(v[0], v[1], v[2]-v[0], v[3]-v[1],
                        angles='xy', scale_units='xy', scale=1, headwidth=3, width=0.001, color='black')
        for v in p_traj:
            if v[0] == v[1] == v[2] == v[3] == 0: continue
            if v[-1] == 0:  color = 'orange'
            else:           color = 'blue'
            plt.quiver(v[0], v[1], v[2]-v[0], v[3]-v[1],
                        angles='xy', scale_units='xy', scale=1, headwidth=3, width=0.001, color=color)
        for v in future_traj:
            if v[0] == v[1] == v[2] == v[3] == 0: continue
            plt.quiver(v[0], v[1], v[2]-v[0], v[3]-v[1],
                        angles='xy', scale_units='xy', scale=1, headwidth=3, width=0.001, color='green')
            
        # limit field of view 
        plt.xlim([-limit_fov, limit_fov])
        plt.ylim([-limit_fov, limit_fov])

    # add legend and show plot
    if show: 
        # Creating legend with color box
        gt = mpatches.Patch(color='green', label='ground truth future')
        focal_track = mpatches.Patch(color='orange', label='ego agent past')
        agents = mpatches.Patch(color='blue', label='agents past')
        lanes = mpatches.Patch(color='black', label='lanes')
        if scene_idx:
            plt.legend(handles=[gt, focal_track, agents, lanes])
        else:
            figure.legend(handles=[gt, focal_track, agents, lanes])
        plt.show()  

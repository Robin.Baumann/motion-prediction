import torch
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from src.visualization.plot_vector_map import plot_vector_map
import src.evaluation.metrics as metrics
import src.utils.coordinate_helper as coordinate_helper


def plot_prediction(scene_idx, model, loader, device, data_dir):

    # go throug data and find scene
    with torch.no_grad():
        scene_found = False
        for i, batch in enumerate(loader):
            if i == scene_idx:
                scene_found = True

                # move to device
                batch = {k: v.to(device) for k, v in batch.items()}

                # pass through model and remove batch dim
                pred, _ = model.predict(batch)

                # get ground truth from batch
                y = batch['y']

                # get coordinates
                pred = coordinate_helper.format_to_coordinates(pred.squeeze())
                y = coordinate_helper.format_to_coordinates(y.squeeze())

                # get fde and ade
                displacements = metrics.calc_l2(pred, y)
                ade, fde = metrics.get_ade(displacements), metrics.get_fde(displacements)

                # plot the vector map but dont display it until rest has been ploted
                plot_vector_map(scene_idx=scene_idx, data_dir=data_dir, show=False)

                # plot prediction
                pred = np.lib.stride_tricks.sliding_window_view(np.concatenate([[[0,0]], pred.cpu()]), (2, 2)).reshape(-1,4)
                for v in pred:
                    plt.quiver(v[0], v[1], v[2]-v[0], v[3]-v[1], angles='xy', scale_units='xy', scale=1, headwidth=3, width=0.001, color='springgreen')

                # limit view 
                limit = 70
                plt.xlim([-limit, limit])
                plt.ylim([-limit, limit])
                
                # Creating legend with color box and display plot
                gt = mpatches.Patch(color='green', label='ground truth future')
                focal_track = mpatches.Patch(color='orange', label='ego agent past')
                agents = mpatches.Patch(color='blue', label='agents past')
                lanes = mpatches.Patch(color='black', label='lanes')
                prediction = mpatches.Patch(color='springgreen', label=f'prediction: ADE {ade:.3f}, FDE {fde:.3f}')
                plt.legend(handles=[gt, focal_track, agents, lanes, prediction])
                plt.show()

        # if no scene found after loop print message
        if not scene_found: print('Scene not found. scene_idx might be out of range.')

import os
from tqdm import tqdm

import torch
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter

from src.evaluation.evaluater import evaluater, eval_batch


def trainer(model, log_dir, checkpoint_dir, train_from_checkpoint, train_loader, val_loader, lr, epochs, weight_decay, device):

    # get number of batches
    batches_in_train_loader = len(train_loader)

    # set start epoch
    start_epoch = 0

    # set optimizer and scheduler
    optimizer = optim.AdamW(model.parameters(), lr=lr, weight_decay=weight_decay)
    scheduler =  optim.lr_scheduler.OneCycleLR(optimizer, max_lr=lr, steps_per_epoch=batches_in_train_loader, epochs=epochs)

    # Load Optimizer and LR scheduler from checkpoint
    if train_from_checkpoint:
        checkpoint = torch.load(train_from_checkpoint)
        start_epoch = checkpoint['epoch']
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        scheduler.load_state_dict(checkpoint['scheduler_state_dict'])
    # create directory for checkpoints
    elif checkpoint_dir:
        os.mkdir(checkpoint_dir)

    # init writer for tensorboard and create directory
    if log_dir:
        os.mkdir(log_dir)
        writer = SummaryWriter(log_dir)

    # start training
    for epoch in range(start_epoch, epochs):

        model.train()
        for i, batch in enumerate(tqdm(train_loader, total=batches_in_train_loader, desc=f"Epoch {epoch+1}")):

            # move to device
            batch = {k: v.to(device) for k, v in batch.items()}
            
            # make optimization step
            optimizer.zero_grad()
            pred, loss = model.predict(batch)
            loss.backward()
            optimizer.step()

            # update learningrate
            scheduler.step()

            # calc metric
            with torch.no_grad():
                train_ade, train_fde, train_miss_rate = eval_batch(pred, batch['y'])

            # write to tensorboard
            if log_dir:
                writer.add_scalars('Miss Rate', {'train': train_miss_rate}, epoch*batches_in_train_loader+i)
                writer.add_scalars('FDE', {'train': train_fde}, epoch*batches_in_train_loader+i)
                writer.add_scalars('ADE', {'train': train_ade}, epoch*batches_in_train_loader+i)
                writer.add_scalars('Loss', {'train': loss.item()}, epoch*batches_in_train_loader+i)
                
        # get metrics for val set
        val_loss, val_ade, val_fde, val_miss_rate = evaluater(model, val_loader, device, desc="evaluating on val set")

        # write loss and metrics to tensorboard
        if log_dir:
            writer.add_scalars('Miss Rate', {'val': val_miss_rate}, (epoch+1)*batches_in_train_loader)
            writer.add_scalars('FDE', {'val': val_fde}, (epoch+1)*batches_in_train_loader)
            writer.add_scalars('ADE', {'val': val_ade}, (epoch+1)*batches_in_train_loader)
            writer.add_scalars('Loss', {'val': val_loss}, (epoch+1)*batches_in_train_loader)
            writer.add_scalar('Learning Rate', scheduler.get_last_lr()[0], (epoch+1)*batches_in_train_loader)

        # print train/val loss and metrics
        print(f"val loss = {val_loss:.3f}\
                \nval Miss Rate = {val_miss_rate:.2f}\
                \nval FDE = {val_fde:.3f}\
                \nval ADE = {val_ade:.3f}")
        
        # Save checkpoint
        if checkpoint_dir:
            checkpoint_path = checkpoint_dir+f"checkpoint_epoch_{epoch+1}.pth"
            torch.save({    'epoch': epoch+1,
                            'model_state_dict': model.state_dict(),
                            'optimizer_state_dict': optimizer.state_dict(),
                            'scheduler_state_dict': scheduler.state_dict(),}, checkpoint_path)
            print(f"Checkpoint saved at {checkpoint_path}")

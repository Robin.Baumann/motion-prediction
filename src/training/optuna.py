import torch
from tqdm import tqdm
import optuna

from src.utils.coordinate_helper import format_to_coordinates
import src.evaluation.metrics as metrics


def optuna_trainer(trial, epochs, device, train_loader, val_loader, model, optimizer, scheduler):

    # start training
    for epoch in tqdm(range(epochs), desc="epochs"):
        model.train()
        for batch in train_loader:

            # move to device
            batch = {k: v.to(device) for k, v in batch.items()}

            # make optimization step
            optimizer.zero_grad()
            _, loss = model.predict(batch)
            loss.backward()
            optimizer.step()

            # update learning rate
            scheduler.step()

        ##################################################################################################################

        # get metrics for val set
        model.eval()
        with torch.no_grad():

            running_ade = 0.0
            for i, batch in enumerate(val_loader):

                # move to device
                batch = {k: v.to(device) for k, v in batch.items()}

                # pass data through model and get best prediction
                out, _ = model.predict(batch)

                # get ground truth from batch
                y = batch['y']

                # calc metrics for every scenes
                running_ade_batch = 0.0
                for scene_idx in range(y.size(0)):

                    # get coordinates
                    x = format_to_coordinates(out[scene_idx])
                    gt = format_to_coordinates(y[scene_idx])

                    # calc all metrics based on l2 distance (only loss and ade when training so safe time)
                    displacements = metrics.calc_l2(x, gt)
                    running_ade_batch += metrics.get_ade(displacements)

                running_ade += running_ade_batch / (scene_idx + 1)

        val_ade = running_ade / (i + 1)

        ##################################################################################################################

        # check if prune trial
        trial.report(val_ade, epoch)
        if trial.should_prune():
            raise optuna.exceptions.TrialPruned()

    return val_ade

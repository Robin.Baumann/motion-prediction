import numpy as np
import math
from numpy.lib.stride_tricks import sliding_window_view
import os
import warnings

os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE" #this was to fix some runtime error i dont remember

########################################################################################################################

# calculate the angles between all gt vectors, given an array in the format [[x_start, y_start, x_end, y_end], [x_start, y_start, x_end, y_end], ...]
def calculate_angles(traj):

    angles = []
    for i in range(1, len(traj)):

        # get vectors
        v1 = traj[i-1]
        v2 = traj[i]

        # norm vectors to origin
        v1 = np.array([v1[2] - v1[0], v1[3] - v1[1]])
        v2 = np.array([v2[2] - v2[0], v2[3] - v2[1]])

        # check if stand still and skip vectors
        if np.all(v1==[0,0]) or np.all(v2==[0,0]): continue

        # calc dot product and norm
        dot_product = np.dot(v1, v2)
        magnitude_v1 = np.linalg.norm(v1)
        magnitude_v2 = np.linalg.norm(v2)

        # calc angle
        cosine_angle = dot_product / (magnitude_v1 * magnitude_v2)

        # fix numeric error
        if math.isclose(cosine_angle, 1, rel_tol=1e-6, abs_tol=0.0):    cosine_angle = 1
        elif math.isclose(cosine_angle, -1, rel_tol=1e-6, abs_tol=0.0): cosine_angle = -1

        # get angle 
        angle = math.acos(cosine_angle)
        angle_degrees = np.degrees(angle)

        # append angle
        angles.append(angle_degrees)

    return angles

########################################################################################################################

#  determie the curvature of a list of vectors based on a 2nd degree polynom that is fitted to the trajectory
def get_curve(traj):

    # get coordinates and norm
    x = np.concatenate((traj[:, 0], [traj[-1, 2]]))
    y = np.concatenate((traj[:, 1], [traj[-1, 3]]))
    x = x/max(x, key=abs)
    y = y/max(y, key=abs)

    # fit function and calc errir for polynomial of order 1
    with warnings.catch_warnings():
        warnings.simplefilter('ignore', np.RankWarning)
        fitted_function = np.poly1d(np.polyfit(x, y, 5))
        fitted_function = fitted_function-fitted_function(x[0])

    # calc derivative and get curve of fitted function by evaluating start and end value 
    der = np.polyder(fitted_function)
    return abs(der(x[0])-der(x[-1]))

########################################################################################################################

# classify a scene based on gt vectors
def get_direction(traj, correct_coordinates=True):

    # format to vector format
    if correct_coordinates:
        traj = sliding_window_view(traj.cpu(), (2, 2)).reshape(-1, 4)

    # filter all padded entries
    mask = (traj[:,0] != 0) | (traj[:,1] != 0) | (traj[:,2] != 0) | (traj[:,3] != 0)
    traj = traj[mask]
    if len(traj) == 0: return "other" # check if all data was filtered and return other

    # get angles, curves, x displacement
    angles = calculate_angles(traj)
    start_angle = np.squeeze(calculate_angles(np.concatenate(([[0,-1,0,0]],traj[:1]))))
    curve = get_curve(traj)
    dx = traj[-1,2] - traj[0,0]
    
    # Classify the trajectory 
    if len(angles) == 0 or np.var(angles) > 100 and start_angle > 10:   	return "other"    
    elif curve < 0.5:                                                       return "straight_drive"
    elif dx > 3:                                                            return "right_turn"
    elif dx < -3:                                                           return "left_turn"
    else:                                                                   return "other"
    
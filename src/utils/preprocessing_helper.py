import os
from tqdm import tqdm
import time
import copy
import numpy as np

########################################################################################################################

# count files and display progressbar
def count_files(directory, num_files_max):

    # create progressbar
    pbar = tqdm(total=num_files_max, desc='processing')

    # init file counts
    count = 0
    last_count = 0

    # check file count as long as not all files have been processed
    while num_files_max > count:

        # reset count after last count
        count = 0
        
        # go through folder and count files
        for file_name in os.listdir(directory):
            if os.path.isfile(os.path.join(directory, file_name)): 
                count += 1
                

        # if count changed update progressbar
        if count > last_count:
            pbar.update(count-last_count)  
            last_count = count

        # sleep for half a second so prcess doesnt run all the time
        time.sleep(.5)

    # close progressbar when finished
    pbar.close()

########################################################################################################################

# create data according to list of data
def create_data(polylines_class, scenes, data_dir, start_scenario, scenarios_per_file):

    # copy class for each process
    polylines = copy.copy(polylines_class)

    # init index for naming the data files last data saved
    data_idx = 0
    idx_last = 0

    # go through data according to chunk size
    for idx in range(scenarios_per_file, len(scenes)+1, scenarios_per_file):

        data = []
        for scene in scenes[idx_last:idx]:

            # Get the Polyline vectors
            p_traj, p_lane, future_traj = polylines.create(scene)

            # add to data
            data.append([p_traj, p_lane, future_traj])

        # save data
        file_name = f"data_{data_idx + (start_scenario // scenarios_per_file)}"
        np.savez_compressed(data_dir+file_name, np.array(data, dtype=object))
        idx_last = idx
        data_idx += 1

########################################################################################################################

# create data according to list of data
def split_for_mulitprocessing(raw_data, dataset, num_processes, num_scenarios_train, num_scenarios_val, scenarios_per_file):

    # limit number of files if passed
    if dataset == 'train/':
        if num_scenarios_train:
            raw_data = raw_data[:num_scenarios_train]
    else:
        if num_scenarios_val:
            raw_data = raw_data[:num_scenarios_val]

    # throw away last few scenes to ensure uniform data length
    discarded_scenes = len(raw_data) % (scenarios_per_file)
    if discarded_scenes != 0:   raw_data = raw_data[:-discarded_scenes]
    print(f'Last {discarded_scenes} scenes discarded to ensure uniform data length.')

    # get number of files that have to be created and print info
    num_files = len(raw_data) // scenarios_per_file
    print(f'{num_files} files will be created with {scenarios_per_file} scenarios per file.')

    # split into equal sizes for every process
    scenario_step = (len(raw_data) // (num_processes * scenarios_per_file) + 1) * scenarios_per_file
    raw_data_split = [raw_data[i:i + scenario_step] for i in range(0, len(raw_data), scenario_step)]

    return raw_data_split, scenario_step, num_files

import os
import torch
import numpy as np
import yaml
import random

from src.models import model as model_class

##################################################################################################################

def set_seed(seed):

    # python
    random.seed(seed)

    # numpy
    np.random.seed(seed)

    # torch
    torch.manual_seed(seed)

    # When running on the CuDNN backend, further options must be set
    if torch.cuda.is_available():
        torch.cuda.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False

    # Set a fixed value for the hash seed
    os.environ["PYTHONHASHSEED"] = str(seed)

##################################################################################################################

def load_config(file_path):

    # create path to config.yml
    config_path = os.path.join(file_path, 'config.yml')

    # load yml file and return dict
    with open(config_path, 'r') as file:
        return yaml.safe_load(file)

##################################################################################################################

def get_model(model_name, predicted_steps, num_predictions, loss_function):

    # get model depeniding on the name 
    if model_name   == 'VectorNet':         model = model_class.VectorNet(predicted_steps=predicted_steps, num_predictions=num_predictions, loss_fun=loss_function)
    elif model_name == 'SimpleLSTM':        model = model_class.SimpleLSTM(predicted_steps=predicted_steps, num_predictions=num_predictions, loss_fun=loss_function)
    elif model_name == 'SimpleTransformer': model = model_class.SimpleTransformer(predicted_steps=predicted_steps, num_predictions=num_predictions, loss_fun=loss_function)
    else:                                   raise NotImplementedError

    return model
from abc import abstractmethod

import torch
import torch.nn as nn
import torch.nn.functional as F

from einops import rearrange

from src.models.vectornet_modules.TrajDecoder import TrajDecoder
from src.models.generic_modules.SelfAttention import SelfAttention
from src.models.generic_modules.CrossAttention import CrossAttention
from src.models.vectornet_modules.Subgraph import SubGraph

from src.models.generic_modules.MLPEncoder import MLPEncoder
from src.models.generic_modules.MLPDecoder import MLPDecoder

import src.utils.coordinate_helper as helper
import src.evaluation.metrics as metrics

########################################################################################################################

class BaseClass(nn.Module):
    def __init__(self, predicted_steps, num_predictions, loss_fun):
        super().__init__()

        # init passed args
        self.loss_fun = loss_fun
        self.predicted_steps = predicted_steps*2 # x,y are concatenated into one dimesionso so x2
        self.K = num_predictions
        self.output_size = self.predicted_steps*self.K

        # if probability is learned by model -> output is mean and std, therefore 2x the output size
        if loss_fun == 'GNLLL':   self.output_size *= 2

    def predict(self, batch):

        # get model output and ground truth
        out =   self.forward(batch)
        y   =   batch['y']

        # train with GNLLL
        if self.loss_fun == 'GNLLL':
            preds, log_std = out.chunk(2, dim=2) # predictions is mean of predicted probabilities
            pred_mask = self.get_pred_mask(preds, y) # get FDEs for all predictions in the current batch and select best ones alonge mode dimension
            pred, log_std = preds[pred_mask], log_std[pred_mask] # filter best predictions according to smallest error
            loss = F.gaussian_nll_loss(input=pred, target=y, var=log_std.exp(), reduction='none').mean()

        # train with MSE loss
        elif self.loss_fun == 'MSE':
            pred_mask = self.get_pred_mask(out, y) # get FDEs for all predictions in the current batch and select best ones alonge mode dimension
            pred = out[pred_mask] # filter best predictions according to smallest error 
            loss = F.mse_loss(input=pred, target=y, reduction='none').mean()

        # train with MSE loss
        elif self.loss_fun == 'SmoothL1':
            pred_mask = self.get_pred_mask(out, y) # get FDEs for all predictions in the current batch and select best ones alonge mode dimension
            pred = out[pred_mask] # filter best predictions according to smallest error 
            loss = F.smooth_l1_loss(input=pred, target=y, reduction='none').mean()

        #  loss not implemented
        else:   raise NotImplementedError

        # return prediction and loss
        return pred, loss

    def get_pred_mask(self, out, y):

        # expand dim of target to k predictions and reshape to [batch_dim, K, ...]
        y = y.repeat(self.K, 1, 1).permute(1, 0, 2)

        fdes_batch = []
        for scene_idx in range(len(y)): # go through each scene in batch

            fdes = []
            for mode_idx in range(self.K): # go through all modes

                # get coordinates
                x_coordinates = helper.format_to_coordinates(out[scene_idx][mode_idx])
                y_coordinates = helper.format_to_coordinates(y[scene_idx][mode_idx])
                
                # calc fde and save
                fdes.append(metrics.get_fde(metrics.calc_l2(x_coordinates, y_coordinates)))

            # append fdes for last scene
            fdes_batch.append(fdes)

        # determine the best prediction
        fde = torch.tensor(fdes_batch)
        fde_best, _ = fde.topk(k=1, dim=1, largest=False) # get the smallest eror

        # create mask for predictions and return
        return fde == fde_best

    @abstractmethod
    def forward(self, x):
       pass

########################################################################################################################

class VectorNet(BaseClass):
    def __init__(self, predicted_steps, num_predictions, loss_fun, num_subgraph_layers=3, hidden_unit=64):
        super().__init__(predicted_steps, num_predictions, loss_fun)

        # init passed args
        self.num_subgraph_layers = num_subgraph_layers
        self.hidden_unit = hidden_unit

        # VectorNet modules (model takes as input features [x_start, y_start, x_end, y_end, polyline type, timestep])
        self.sub_graph = SubGraph(  in_channels=6,
                                    num_subgraph_layers=self.num_subgraph_layers,
                                    hidden_unit=self.hidden_unit)

        self.self_attention = SelfAttention(    in_channels=self.hidden_unit * 2,
                                                hidden_unit=self.hidden_unit,
                                                bias=True,
                                                scaling=False)
                                                                
        self.traj_decoder = TrajDecoder(    in_channels=self.hidden_unit,
                                            hidden_unit=self.hidden_unit,
                                            out_channels=self.output_size)

    def forward(self, input_data):
        # input_data:   x, cluster
        # x:            [batch_dim, input_features]
        # cluster:      [batch_dim, index]

        # input data consists of features and cluster
        x, cluster = input_data['x'], input_data['cluster']

        # pass through VectorNet layer
        x = self.sub_graph(x, cluster)  # pass through subgraph
        x = self.self_attention(x)  # global graph attention
        x = self.traj_decoder(x[:, 0])  # decode 0th polyline for prediction as polyline 0 is ego agent
        return x.view(x.shape[0], self.K, -1)  # reshape to [batch_dim, num_predictions, ...]

########################################################################################################################

class SimpleLSTM(BaseClass):
    def __init__(self, predicted_steps, num_predictions, loss_fun, num_lstm_layer=3, hidden_unit=128):
        super().__init__(predicted_steps, num_predictions, loss_fun)
        
        # init passed args
        self.num_lstm_layer = num_lstm_layer
        self.hidden_unit = hidden_unit

        # modules (model takes as input features [x_start, y_start, x_end, y_end])
        self.encoder = MLPEncoder(  in_channels=4,
                                    output_size=self.hidden_unit)

        self.lstm = nn.LSTM(input_size=self.hidden_unit,
                            hidden_size=self.hidden_unit,
                            num_layers=self.num_lstm_layer,
                            batch_first=True)

        self.decoder = MLPDecoder(  in_channels=self.hidden_unit,
                                    output_size=self.output_size)

    def forward(self, input_data):
        # input data: x = [batch_dim, num_agents, seq_len, input_features]
        
        # input data is only agent data
        x = input_data['x']

        # Pass the input sequences through decoder
        x = self.encoder(x)

        # reshape input to span agents along batch dimensions
        x = rearrange(x, 'b a t f -> (b a) t f')

        # reshape x so agents are flattend over batch dim and pass through lstm layer
        _, (hidden_state, _) = self.lstm(x)

        # reshape agents back to [batch_size, num_agents, hidden_size]
        x = rearrange(hidden_state[-1], '(b a) h -> b a h', b=input_data['x'].shape[0])

        # decode trajectory but only agent 0 wich is focal agent
        x = self.decoder(x[:, 0])
        
        return x.view(x.shape[0], self.K, -1)  # reshape to [batch_dim, num_predictions, ...]

########################################################################################################################

class SimpleTransformer(BaseClass):
    def __init__(self, predicted_steps, num_predictions, loss_fun, num_encoder_decoder_layer=3, num_heads=4, hidden_unit=128):
        super().__init__(predicted_steps, num_predictions, loss_fun)

        # init passed args
        self.num_encoder_decoder_layer = num_encoder_decoder_layer
        self.num_heads = num_heads
        self.hidden_unit = hidden_unit

        # model takes as input features [x_start, y_start, x_end, y_end])
        self.embedding_layer = nn.Linear(   in_features=4,
                                            out_features=self.hidden_unit)

        self.transformer = nn.Transformer(  d_model=self.hidden_unit, 
                                            nhead=self.num_heads, 
                                            num_encoder_layers=self.num_encoder_decoder_layer,
                                            num_decoder_layers=0, 
                                            dim_feedforward=self.hidden_unit,
                                            norm_first=True,
                                            batch_first=True)
    
        self.decoder = MLPDecoder(  in_channels=self.hidden_unit,
                                    output_size=self.output_size)
        
    def forward(self, input_data):
        # input data: x = [batch_dim, num_agents, seq_len, input_features]
        
        # input data is only agent data
        x = rearrange(input_data['x'], 'b a t f -> (b a) t f')

        # pass through embedding to change size
        x = self.embedding_layer(x)
        
        # pass through transformer
        x = self.transformer.encoder(x)

        # reshape agents back to [batch_size, num_agents, input_size]
        x = rearrange(x, '(b a) s f -> b a s f', b=input_data['x'].shape[0])
        x = x.mean(dim=2) # build mean over sequence length to generate output

        # decode trajectory but only agent 0 wich is focal agent
        x = self.decoder(x[:, 0])
        
        return x.view(x.shape[0], self.K, -1)  # reshape to [batch_dim, num_predictions, ...]
    
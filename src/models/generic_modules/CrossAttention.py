import torch
import torch.nn as nn
import torch.nn.functional as F


class CrossAttention(nn.Module):
    def __init__(self, query_channels, key_value_channels, hidden_unit, bias=False, scaling=True):
        super().__init__()

        self.query_channels = query_channels
        self.key_value_channels = key_value_channels
        self.hidden_unit = hidden_unit
        self.bias = bias
        self.scaling = scaling

        # Define attention layers
        self.q_lin = nn.Linear(self.query_channels, self.hidden_unit, bias=self.bias)
        self.k_lin = nn.Linear(self.key_value_channels, self.hidden_unit, bias=self.bias)
        self.v_lin = nn.Linear(self.key_value_channels, self.hidden_unit, bias=self.bias)

    def forward(self, query, key_value):

        query = self.q_lin(query)
        key = self.k_lin(key_value)
        value = self.v_lin(key_value)

        scores = torch.bmm(query, key.transpose(1, 2))
        if self.scaling: scores /= self.hidden_unit**0.5
        attention_weights = F.softmax(scores, dim=-1)
        return torch.bmm(attention_weights, value)

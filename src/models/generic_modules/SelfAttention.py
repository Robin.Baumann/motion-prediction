import torch
import torch.nn as nn
import torch.nn.functional as F


class SelfAttention(nn.Module):
    def __init__(self, in_channels, hidden_unit, bias=False, scaling=True):
        super().__init__()

        self.in_channels = in_channels
        self.hidden_unit = hidden_unit
        self.bias = bias
        self.scaling = scaling

        # define attention layer
        self.q_lin = nn.Linear(self.in_channels, self.hidden_unit, bias=self.bias)
        self.k_lin = nn.Linear(self.in_channels, self.hidden_unit, bias=self.bias)
        self.v_lin = nn.Linear(self.in_channels, self.hidden_unit, bias=self.bias)

    def forward(self, x):

        query = self.q_lin(x)
        key = self.k_lin(x)
        value = self.v_lin(x)

        scores = torch.bmm(query, key.transpose(1, 2))
        if self.scaling: scores /= self.hidden_unit**0.5
        attention_weights = F.softmax(scores, dim=-1)
        return torch.bmm(attention_weights, value)

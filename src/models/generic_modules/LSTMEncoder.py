import torch
import torch.nn as nn

class LSTMEncoder(nn.Module):
    def __init__(self, in_channels, hidden_unit, num_lstm_layer):
        super().__init__()

        # input args
        self.in_channels = in_channels
        self.hidden_unit = hidden_unit
        self.num_lstm_layer = num_lstm_layer

        # LSTM Layer
        self.lstm = nn.LSTM(input_size=self.in_channels, 
                            hidden_size=self.hidden_unit, 
                            num_layers=self.num_lstm_layer, 
                            batch_first=True)

    def forward(self, x):

        # pass through LSTM
        _, (hidden_state, _) = self.lstm(x)

        # only take last timestep and reshape back
        return hidden_state[-1]
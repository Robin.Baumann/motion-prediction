'''
currently implemented packages:
    torch 
    torchvision 
    torchaudio
    torch_geometric
    torch_scatter
    torch_sparse
    torch_cluster
    torch_spline_conv
    optuna
    tensorboard
    einops
    av2
    nuscenes-devkit
    argoverse-api
'''

########################################################################################################################

import subprocess

########################################################################################################################

# Help message for CUDA version input
cuda_help_message = (
    "Enter your CUDA version. "
    "The CUDA version should be in the format cuXXX "
    "(e.g., cu118 for CUDA 11.8). If you're unsure, you can check your CUDA version "
    "using the 'nvcc --version' command in your terminal."
)

# Prompt user for CUDA version
cuda_version = input(cuda_help_message + "\nCUDA version: ")

########################################################################################################################

# Dependencies and their installation commands
dependencies = {
    "PyTorch": \
        f"pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/{cuda_version}",

    "Torch Geometric": \
        "pip install torch_geometric",

    "Torch Geometric Optional Dependencies": \
        f"pip install torch_scatter torch_sparse torch_cluster torch_spline_conv -f https://data.pyg.org/whl/torch-2.0.1+{cuda_version}.html",

    "Additional Packages": \
        "pip install optuna tensorboard einops",

    "Dataset APIs": \
        "pip install av2 nuscenes-devkit",

    "Downgrading numpy to install argoverse-api\n(error can be ignored)": \
        "pip install numpy==1.19.0", # this is done because the argoverse-api can only be installed with and older numpy version

    "Argoverse API\n(error can be ignored)": \
        "pip install git+https://github.com/argoai/argoverse-api.git",

    "Upgrading numpy again\n(error can be ignored)": \
        "pip install numpy --upgrade" # numpy can just be updated again after api is installed
}

########################################################################################################################

# Install dependencies
for description, command in dependencies.items():

    # print sep strings and description
    print('\n\n')
    print('#'*100)
    print('\n')
    print(description, '\n\n')

    # run installatin
    subprocess.run(command, shell=True, check=True)

# print sep strings and let user know all installations have been done
print('\n\n')
print('#'*100)
print('\nDone.\n')

import os
import os.path as osp
import numpy as np
import argparse
from tqdm import tqdm

from src.data.dataset import PolylineDataset
import src.utils.scene_classification as scene_classification
from src.utils.setup_helper import set_seed, load_config

########################################################################################################################

# load config
CONFIG = load_config(os.path.dirname(os.path.abspath(__file__)))

########################################################################################################################

# Define the parser
parser = argparse.ArgumentParser()
parser.add_argument('--dataset', type=str, default=None)

parser.add_argument('--seed', type=float, default=None)

parser.add_argument('--data_mini', type=bool, default=CONFIG['dataset_option']['data_mini'])
parser.add_argument('--scenarios_per_file', type=int, default=CONFIG['preprocessing']['scenarios_per_file'])
parser.add_argument('--eval_dataset', type=str, default=CONFIG['train_eval_options']['eval_dataset'])
args = parser.parse_args()

# check must have args
if not args.dataset: raise Exception('Dataset must be defined with --dataset')

##################################################################################################################

# set directory and name
data_dir = CONFIG['directory']['path']+args.dataset+'/'+'processed_data/'
if args.data_mini: data_dir = data_dir[:-1]+'_mini/'

##################################################################################################################

# Random Seed
set_seed(args.seed if args.seed else CONFIG['seed'])

########################################################################################################################

data_set = PolylineDataset(data_dir + args.eval_dataset+'/', None, None, False)

########################################################################################################################

# get all file names in data directory
paths = []
for file_name in os.listdir(data_dir + args.eval_dataset+'/'):
    if 'data' in file_name: 
        paths.append(osp.join(data_dir + args.eval_dataset+'/', file_name))

# get file size
file_size = len(np.load(paths[0], allow_pickle=True)['arr_0'])

########################################################################################################################

scenes_found = False

# list of directions to evaluate (None means all data will be evaluated) 
directions = ["right_turn", "left_turn", "straight_drive"]

for direction in directions:

    # create empty arry for scenes
    scenes = []

    # set directory to save data to and create folder if not done already
    save_dir = data_dir + args.eval_dataset+'-'+direction+'/'
    if not os.path.exists(save_dir): os.mkdir(save_dir)
    
    # create list of indices representing all scenes and shuffle
    all_scenes = np.arange(len(paths)*file_size)
    np.random.shuffle(all_scenes)

    # go through all scenes and search for directions
    for scene in tqdm(all_scenes, desc=f'Searching for {direction} scenes'):

        # get ground truth and check direction
        p_traj, p_lane, y = data_set.get_raw(scene)
        if direction == scene_classification.get_direction(y[:,:4], correct_coordinates=False): 
            scenes.append([p_traj, p_lane, y])
    
        # save data if enough has been found
        if len(scenes) == args.scenarios_per_file: 
            
            # save data
            scenes_found = True
            np.savez_compressed(save_dir+"data", np.array(scenes, dtype=object))
            scenes = []             # reset scenes
            break
    
    if scenes_found:
        print(f'{args.scenarios_per_file} Scenes found and saved.')
        print('-'*30)
    else: 
        print(f'Not enough scenes found. No data was saved. Try to set scenarios_per_file lower.')
        print('-'*30)
    

import os
import torch
from torch.utils.data import DataLoader
import torch.optim as optim
import numpy as np
import optuna
import argparse
from time import strftime, localtime

from src.data.dataset import PolylineDataset, ShuffleBatchSampler, collate_fn
from src.utils.setup_helper import set_seed, load_config, get_model
from src.training.optuna import optuna_trainer

########################################################################################################################

# load config
CONFIG = load_config(os.path.dirname(os.path.abspath(__file__)))

##################################################################################################################

# Define the parser
parser = argparse.ArgumentParser()
parser.add_argument('--model', type=str, default=None)
parser.add_argument('--dataset', type=str, default=None)

parser.add_argument('--seed', type=float, default=None)

parser.add_argument('--epochs', type=int, default=None)
parser.add_argument('--batch_size', type=int, default=None)
parser.add_argument('--lr', type=float, default=None)
parser.add_argument('--weight_decay', type=float, default=None)

parser.add_argument('--loss_function', type=str, default=None)
parser.add_argument('--num_predictions', type=int, default=None)

parser.add_argument('--input_sec', type=int, default=None)
parser.add_argument('--pred_sec', type=int, default=None)

parser.add_argument('--data_mini', type=bool, default=CONFIG['dataset_option']['data_mini'])
parser.add_argument('--reduce_dataset_size', type=float, default=CONFIG['dataset_option']['reduce_dataset_size_percent'])
parser.add_argument('--num_workers', type=int, default=CONFIG['dataset_option']['dataloader_num_workers'])
parser.add_argument('--log', type=bool, default=CONFIG['train_eval_options']['log_tensorboard'])
parser.add_argument('--gpu', type=int, default=CONFIG['train_eval_options']['gpu_num'])
parser.add_argument('--num_trials', type=int, default=CONFIG['optuna']['num_trials'])
args = parser.parse_args()

# check must have params
if not args.model: raise Exception('Model must be defined with --model')
if not args.dataset: raise Exception('Dataset must be defined with --dataset')

# set hyperparams based on model and dataset
epochs = args.epochs if args.epochs else CONFIG['hyperparameter'][args.model][args.dataset]['epochs']
batch_size = args.batch_size if args.batch_size else CONFIG['hyperparameter'][args.model][args.dataset]['batch_size']
lr = args.lr if args.lr else CONFIG['hyperparameter'][args.model][args.dataset]['lr']
weight_decay = args.weight_decay if args.weight_decay else CONFIG['hyperparameter'][args.model][args.dataset]['weight_decay']

loss_function = args.loss_function if args.loss_function else CONFIG['hyperparameter'][args.model]['loss_function']
num_predictions = args.num_predictions if args.num_predictions else CONFIG['hyperparameter'][args.model]['num_predictions']

# set input and output lenghts and sample rate
input_len = args.input_sec if args.input_sec else CONFIG['dataset_meta_data'][args.dataset]['prediction_history']
if input_len > CONFIG['dataset_meta_data'][args.dataset]['prediction_history']: raise Exception('input length exceeds length available in the dataset')
pred_len = args.pred_sec if args.pred_sec else CONFIG['dataset_meta_data'][args.dataset]['prediction_horizon']
if pred_len > CONFIG['dataset_meta_data'][args.dataset]['prediction_horizon']: raise Exception('prediction length exceeds length available in the dataset')
hz = CONFIG['dataset_meta_data'][args.dataset]['sample_rate']

# set directory and name
data_dir = CONFIG['directory']['path']+args.dataset+'/'+'processed_data/'
if args.data_mini: data_dir = data_dir[:-1]+'_mini/'

##################################################################################################################

# Random Seed
set_seed(args.seed if args.seed else CONFIG['seed'])

##################################################################################################################

# load datasets
train_set = PolylineDataset(data_dir+'train/', args.model, input_len*hz, pred_len*hz, args.reduce_dataset_size)
val_set = PolylineDataset(data_dir+'val/', args.model, input_len*hz, pred_len*hz, args.reduce_dataset_size)

# print how much of the dataset is used
if args.reduce_dataset_size:    print(f'{args.reduce_dataset_size}% of dataset used')
else:                           print('100% of dataset used')

##################################################################################################################

# get device
device = torch.device(f'cuda:{args.gpu}' if torch.cuda.is_available() else 'cpu')
print(f'Device: {device}')

##################################################################################################################

# Trial function
print('Start Trial ...')
def objective(trial):

    # define trial for hyperparams
    lr = trial.suggest_float('lr', 1e-6, 1e-2, log=True)
    weight_decay = trial.suggest_float('weight_decay', 1e-6, 1e-2, log=True)

    # create dataloaders
    sampler = ShuffleBatchSampler(np.arange(len(train_set)), batch_size)
    train_loader = DataLoader(train_set, batch_size=batch_size, shuffle=False, num_workers=args.num_workers, collate_fn=collate_fn, sampler=sampler)
    val_loader = DataLoader(val_set, batch_size=batch_size, shuffle=False, num_workers=args.num_workers, collate_fn=collate_fn)

    # load model and model to available device
    model = get_model(args.model, pred_len*hz, num_predictions, loss_function)
    model = model.to(device)

    # set optimizer
    optimizer = optim.AdamW(model.parameters(), lr=lr, weight_decay=weight_decay)
    scheduler =  optim.lr_scheduler.OneCycleLR(optimizer, max_lr=lr, steps_per_epoch=len(train_loader), epochs=epochs)

    # start training and return result
    return optuna_trainer(trial, epochs, device, train_loader, val_loader, model, optimizer, scheduler)

##################################################################################################################

study = optuna.create_study(direction="minimize", sampler=optuna.samplers.TPESampler(), pruner=optuna.pruners.HyperbandPruner())
study.optimize(objective, n_trials=args.num_trials)

##################################################################################################################

# get results
pruned_trials = study.get_trials(deepcopy=False, states=[optuna.trial.TrialState.PRUNED])
complete_trials = study.get_trials(deepcopy=False, states=[optuna.trial.TrialState.COMPLETE])

# write results to file
time_str = f'{strftime("%Y-%m-%d_%H-%M", localtime())}'
file_name = f'./logs/hyperparam_search_{args.dataset}_{args.model}_{time_str}.txt'
with open(file_name, 'w') as outfile:
    
    outfile.write(f"Study statistics: \n")
    outfile.write(f"  Number of finished trials: {len(study.trials)}\n")
    outfile.write(f"  Number of pruned trials: {len(pruned_trials)}\n")
    outfile.write(f"  Number of complete trials: {len(complete_trials)}\n")
    outfile.write(f"Best trial: \n")
    trial = study.best_trial
    outfile.write(f"  Value: {trial.value}\n")
    outfile.write(f"  Params: \n")
    for key, value in trial.params.items():
        outfile.write(f"    {key}: {value}\n")
        
#!/bin/bash

# Folder structure
mkdir -p ./data/argoverse/raw_data
cd ./data/argoverse/raw_data

# Download
wget https://s3.amazonaws.com/argoverse/datasets/av1.1/tars/forecasting_train_v1.1.tar.gz
wget https://s3.amazonaws.com/argoverse/datasets/av1.1/tars/forecasting_val_v1.1.tar.gz
wget https://s3.amazonaws.com/argoverse/datasets/av1.1/tars/forecasting_test_v1.1.tar.gz
wget https://s3.amazonaws.com/argoverse/datasets/av1.1/tars/hd_maps.tar.gz

# Extract
tar xvf forecasting_train_v1.1.tar.gz
tar xvf forecasting_val_v1.1.tar.gz
tar xvf forecasting_test_v1.1.tar.gz
tar xf hd_maps.tar.gz

# Argoverse package needs the map_files folder
SITE_PACKAGES=$(python -c 'import site; print(site.getsitepackages()[0])')
cp -r map_files $SITE_PACKAGES

# remove files
tar forecasting_train_v1.1.tar.gz
tar forecasting_val_v1.1.tar.gz
tar forecasting_test_v1.1.tar.gz
tar hd_maps.tar.gz
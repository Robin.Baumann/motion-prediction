import os
import re
import torch
from torch.utils.data import DataLoader
import numpy as np
import argparse

from src.data.dataset import PolylineDataset, collate_fn
from src.evaluation.evaluater import evaluater
from src.utils.setup_helper import set_seed, load_config

########################################################################################################################

# load config
CONFIG = load_config(os.path.dirname(os.path.abspath(__file__)))

########################################################################################################################

# Define the parser
parser = argparse.ArgumentParser()
parser.add_argument('--trained_model',  type=str, default=None)

parser.add_argument('--model_path',  type=str, default=None)
parser.add_argument('--save_eval', type=bool, default=CONFIG['train_eval_options']['save_eval'])
parser.add_argument('--save_path',  type=str, default=None)
parser.add_argument('--seed', type=float, default=None)

parser.add_argument('--data_mini', type=bool, default=CONFIG['dataset_option']['data_mini'])
parser.add_argument('--reduce_dataset_size', type=float, default=CONFIG['dataset_option']['reduce_dataset_size_percent'])
parser.add_argument('--num_workers', type=int, default=CONFIG['dataset_option']['dataloader_num_workers'])
parser.add_argument('--batch_size', type=int, default=CONFIG['train_eval_options']['eval_batch_size'])
parser.add_argument('--eval_dataset', type=str, default=CONFIG['train_eval_options']['eval_dataset'])
parser.add_argument('--eval_by_direction', type=bool, default=CONFIG['train_eval_options']['eval_by_direction'])
args = parser.parse_args()

# check must have args
if not args.trained_model: raise Exception('Saved model must be defined with --trained_model')

# get number of predictions and dataset name
model_name = args.trained_model.split("-")[0]
dataset = args.trained_model.split("-")[3]

model_path = args.model_path if args.model_path else './models/'
save_path = args.save_path if args.save_path else './logs/evaluations/'

# set directory and name
data_dir = CONFIG['directory']['path']+dataset+'/'+'processed_data/'
if args.data_mini: data_dir = data_dir[:-1]+'_mini/'

##################################################################################################################

# Random Seed
set_seed(args.seed if args.seed else CONFIG['seed'])

########################################################################################################################

# get device
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(f'Device: {device}')

########################################################################################################################

# load model
model = torch.load(model_path+args.trained_model)
model = model.to(device)

########################################################################################################################

# list of directions to evaluate (None means all data will be evaluated) 
if args.eval_by_direction:  directions = ["right_turn", "left_turn", "straight_drive", None]
else:                       directions = [None]

# list of evaluation metrics
val_loss_list, val_ade_list, val_fde_list, val_miss_rate_list, num_scenes_list = [], [], [], [], [] 

# evaluate for all directions
for direction in directions:

    print(f"Evaluating {'complete dataset' if direction==None else direction}:")

    ########################################################################################################################

    # get sample rate of dataset
    hz = CONFIG['dataset_meta_data'][dataset]['sample_rate']
    input_len = int(re.search(r'(\d+)s(\d+)s', args.trained_model).group(1))
    pred_len = int(re.search(r'(\d+)s(\d+)s', args.trained_model).group(2))

    # set dataset path
    if direction == None:   eval_data_dir = data_dir + args.eval_dataset+'/'
    else:                   eval_data_dir = data_dir + args.eval_dataset+'-'+direction+'/'

    # create datasets
    data_set = PolylineDataset(eval_data_dir, model_name, input_len*hz, pred_len*hz, args.reduce_dataset_size)
    data_loader = DataLoader(data_set, batch_size=args.batch_size, shuffle=False, num_workers=args.num_workers, collate_fn=collate_fn)

    ##################################################################################################################

    # evaluate model
    val_loss, val_ade, val_fde, val_miss_rate = evaluater(  model=model,
                                                            device=device,
                                                            data_loader=data_loader)
    # save all metrics
    val_loss_list.append(np.array(val_loss))
    val_ade_list.append(np.array(val_ade.cpu()))
    val_fde_list.append(np.array(val_fde.cpu()))
    val_miss_rate_list.append(np.array(val_miss_rate))

    # print metrics
    print(f"Loss:\t\t{val_loss:.3f}")
    print(f"ADE:\t\t{val_ade:.3f}")
    print(f"FDE:\t\t{val_fde:.3f}")
    print(f"Miss Rate:\t{val_miss_rate:.3f}")

    # save metrics if passed
    if args.save_eval:
        evaluation = np.concatenate((val_loss_list, val_ade_list, val_fde_list, val_miss_rate_list, num_scenes_list))
        file_name = f'evaluation-{args.trained_model[:-4]}'
        np.save(save_path+file_name, evaluation)
        print(f'Evaluation metrics saved to : ./logs/evaluations/{file_name}')

import os
import re
import argparse
import torch
from torch.utils.data import DataLoader
from src.data.dataset import PolylineDataset, collate_fn

from src.visualization.plot_vector_map import plot_vector_map
from src.visualization.plot_prediction import plot_prediction
from src.utils.setup_helper import load_config

########################################################################################################################

# load config
CONFIG = load_config(os.path.dirname(os.path.abspath(__file__)))

##################################################################################################################

# Define the parser
parser = argparse.ArgumentParser()
parser.add_argument('--dataset', type=str, default=None)
parser.add_argument('--model', type=str, default=None)

parser.add_argument('--data_mini', type=bool, default=CONFIG['dataset_option']['data_mini'])
parser.add_argument('--vector_map', type=bool, default=CONFIG['plot_options']['plot_vector_map'])
parser.add_argument('--prediction', type=str, default=CONFIG['plot_options']['plot_prediction'])
parser.add_argument('--scene_idx', type=int, default=CONFIG['plot_options']['viz_scene_idx'])
parser.add_argument('--dataset_split', type=str, default=CONFIG['plot_options']['viz_dataset'])
args = parser.parse_args()

# check must have args
if not args.dataset: raise Exception('Dataset must be defined with --dataset')

# set directory
data_dir = CONFIG['directory']['path']+args.dataset+'/'+'processed_data/'
if args.data_mini: data_dir = data_dir[:-1]+'_mini/'
data_dir_raw = CONFIG['directory']['path']+args.dataset+'/'+CONFIG['directory']['raw']


##################################################################################################################

# plot generated vector map
if args.vector_map:
    plot_vector_map(scene_idx=args.scene_idx, data_dir=data_dir+args.dataset_split+'/')

##################################################################################################################
CONFIG['dataset_meta_data'][args.dataset]['prediction_history']
CONFIG['dataset_meta_data'][args.dataset]['prediction_horizon']
# plot vector map with model predictions
if args.prediction:

    # check if model name was passed
    if not args.model: raise Exception('Model must be defined with --model when using --prediction')

    # get sample rate of dataset
    hz = CONFIG['dataset_meta_data'][args.dataset]['sample_rate']
    input_len = int(re.search(r'(\d+)s(\d+)s', args.prediction).group(1))
    pred_len = int(re.search(r'(\d+)s(\d+)s', args.prediction).group(2))

    # load model and move to device
    model = torch.load('./models/'+args.prediction)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = model.to(device)

    # get dataset and dataloader
    data = PolylineDataset(data_dir+args.dataset_split+'/', args.model, input_len*hz, pred_len*hz)
    loader = DataLoader(data, batch_size=1, shuffle=False, collate_fn=collate_fn)

    # plot vector map and prediction
    plot_prediction(scene_idx=args.scene_idx, model=model, loader=loader, device=device, data_dir=data_dir+args.dataset_split+'/')

## Preparation
- Run the following commands in the wsl terminal:
    - ``sudo apt-get update``
    - ``sudo apt-get install build-essential freeglut3-dev libx11-dev libxmu-dev libxi-dev libglu1-mesa-dev``

## CUDA
- Select desired cuda version on [cuda-toolkit-archive](https://developer.nvidia.com/cuda-toolkit-archive)
- Select Linux -> x86_64 -> WSL-Ubuntu -> deb(local)
- Execute all displayed commands in the wsl terminal
- Afterwards execute the following command to add cuda to the system PATH:
    - ``export PATH=/usr/local/cuda/bin:$PATH``

## CUDNN
- Download the latest _Local Installer for Linux x86_64 (Tar)_  from [downloaden](https://developer.nvidia.com/rdp/cudnn-archive) for your cuda version
- Extract the files 
- Rename the folder to __cuda__ (just for ease of use, as the folder name is quite large)
- Open a wsl terminal at the location of the extracted folder
- Copy the cuDNN header files (cudnn*.h) to your wsl cuda directory by executing:
    - ``sudo cp cuda/include/cudnn*.h /usr/local/cuda/include/``
- Copy the the cuDNN library files (libcudnn*) to your wsl cuda directory by executing:
    - ``sudo cp cuda/lib/libcudnn* /usr/local/cuda/lib64/``
- Change permissions of the copied header and library files to become readable by all users on the system
    - ``sudo chmod a+r /usr/local/cuda/include/cudnn*.h /usr/local/cuda/lib64/libcudnn*``
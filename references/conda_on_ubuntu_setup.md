## Anaconda for Ubuntu
- Check newest Version in [Archive](https://repo.continuum.io/archive)
- adjust the version for the following commands if necessary
- Run the following commands in the terminal:
    - ``wget https://repo.continuum.io/archive/Anaconda3-2022.10-Linux-x86_64.sh``
    - ``bash Anaconda3-2022.10-Linux-x86_64.sh``
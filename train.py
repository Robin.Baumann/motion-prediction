import os
import yaml
import torch
from torch.utils.data import DataLoader
import numpy as np
import time
from time import strftime, localtime
import argparse

from src.data.dataset import PolylineDataset, ShuffleBatchSampler, collate_fn
from src.training.trainer import trainer
from src.utils.setup_helper import set_seed, load_config, get_model

########################################################################################################################

# load config
CONFIG = load_config(os.path.dirname(os.path.abspath(__file__)))

##################################################################################################################

# Define the parser
parser = argparse.ArgumentParser()
parser.add_argument('--model', type=str, default=None)
parser.add_argument('--dataset', type=str, default=None)

parser.add_argument('--epochs', type=int, default=None)
parser.add_argument('--batch_size', type=int, default=None)
parser.add_argument('--lr', type=float, default=None)
parser.add_argument('--weight_decay', type=float, default=None)

parser.add_argument('--seed', type=float, default=None)
parser.add_argument('--save_path',  type=str, default=None)

parser.add_argument('--loss_function', type=str, default=None)
parser.add_argument('--num_predictions', type=int, default=None)

parser.add_argument('--input_sec', type=int, default=None)
parser.add_argument('--pred_sec', type=int, default=None)

parser.add_argument('--data_mini', type=bool, default=CONFIG['dataset_option']['data_mini'])
parser.add_argument('--reduce_dataset_size', type=float, default=CONFIG['dataset_option']['reduce_dataset_size_percent'])
parser.add_argument('--num_workers', type=int, default=CONFIG['dataset_option']['dataloader_num_workers'])
parser.add_argument('--log', type=bool, default=CONFIG['train_eval_options']['log_tensorboard'])
parser.add_argument('--checkpoints', type=bool, default=CONFIG['train_eval_options']['checkpoints'])
parser.add_argument('--train_from_checkpoint', type=str, default=CONFIG['train_eval_options']['train_from_checkpoint'])
parser.add_argument('--finetune', type=str, default=CONFIG['train_eval_options']['finetune_model'])
parser.add_argument('--gpu', type=int, default=CONFIG['train_eval_options']['gpu_num'])
args = parser.parse_args()

# check necessary args
if not args.model: raise Exception('Model must be defined with --model')
if not args.dataset: raise Exception('Dataset must be defined with --dataset')

# set path to save trained models to
save_path = args.save_path if args.save_path else './models/'

# set hyperparams based on model and dataset
epochs = args.epochs if args.epochs else CONFIG['hyperparameter'][args.model][args.dataset]['epochs']
batch_size = args.batch_size if args.batch_size else CONFIG['hyperparameter'][args.model][args.dataset]['batch_size']
lr = args.lr if args.lr else CONFIG['hyperparameter'][args.model][args.dataset]['lr']
weight_decay = args.weight_decay if args.weight_decay else CONFIG['hyperparameter'][args.model][args.dataset]['weight_decay']

loss_function = args.loss_function if args.loss_function else CONFIG['hyperparameter'][args.model]['loss_function']
num_predictions = args.num_predictions if args.num_predictions else CONFIG['hyperparameter'][args.model]['num_predictions']

# set input and output lenghts and sample rate
input_len = args.input_sec if args.input_sec else CONFIG['dataset_meta_data'][args.dataset]['prediction_history']
if input_len > CONFIG['dataset_meta_data'][args.dataset]['prediction_history']: raise Exception('input length exceeds length available in the dataset')
pred_len = args.pred_sec if args.pred_sec else CONFIG['dataset_meta_data'][args.dataset]['prediction_horizon']
if pred_len > CONFIG['dataset_meta_data'][args.dataset]['prediction_horizon']: raise Exception('prediction length exceeds length available in the dataset')
hz = CONFIG['dataset_meta_data'][args.dataset]['sample_rate']
ident = f'{input_len}s{pred_len}s'

# set directory
data_dir = CONFIG['directory']['path']+args.dataset+'/'+'processed_data/'
if args.data_mini: data_dir = data_dir[:-1]+'_mini/'

##################################################################################################################

# Random Seed
set_seed(args.seed if args.seed else CONFIG['seed'])

##################################################################################################################

# load datasets
train_set = PolylineDataset(data_dir+'train/', args.model, input_len*hz, pred_len*hz, args.reduce_dataset_size)
val_set = PolylineDataset(data_dir+'val/', args.model, input_len*hz, pred_len*hz, args.reduce_dataset_size)

# only use part of dataset if passed
print(f'{args.reduce_dataset_size if args.reduce_dataset_size else 100.0}% of dataset used')

##################################################################################################################

# create dataloaders
sampler = ShuffleBatchSampler(np.arange(len(train_set)), batch_size)
train_loader = DataLoader(train_set, batch_size=batch_size, shuffle=False, num_workers=args.num_workers, collate_fn=collate_fn, sampler=sampler)
val_loader = DataLoader(val_set, batch_size=batch_size, shuffle=False, num_workers=args.num_workers, collate_fn=collate_fn)

##################################################################################################################

# get device
device = torch.device(f'cuda:{args.gpu}' if torch.cuda.is_available() else 'cpu')
print(f'Device: {device}')

##################################################################################################################

# load model
model = get_model(args.model, pred_len*hz, num_predictions, loss_function)

##################################################################################################################

# use pretrained model and finetune all layers
if args.finetune:
    # load existing trained model
    model = torch.load(save_path+args.finetune)
    # add string to ident that indicated what model was finetuned
    ident = 'finetuned_'+str.split(args.finetune, '-')[-2]+'_'+str.split(args.finetune, '-')[-1][:-4]+'-'+ident
    # set learning rate to 10% of default lr for finetuning
    lr *= 0.1

##################################################################################################################

# move model to available device
model = model.to(device)

##################################################################################################################

# set time string
time_str = f'{strftime("%Y-%m-%d_%H-%M", localtime())}'

# set model name
model_str = f'{args.model}-{loss_function}-K{num_predictions}-{args.dataset}-{ident}-{args.reduce_dataset_size if args.reduce_dataset_size else 100.0}-{time_str}'

##################################################################################################################

# create tensorboard log directory
if args.log:
    log_dir = f'./logs/tensorboard/'+model_str+'/'
    print(f'Tensorboard logs will be saved to: {log_dir}')
else:   log_dir = None

##################################################################################################################

# create tensorboard log directory
if args.checkpoints:
    if args.train_from_checkpoint:
        train_from_checkpoint_path = './logs/checkpoints/'+args.train_from_checkpoint
        checkpoint_dir = os.path.dirname(os.path.abspath(train_from_checkpoint_path))+'/'
    else:
        train_from_checkpoint_path = args.train_from_checkpoint
        checkpoint_dir = './logs/checkpoints/'+model_str+'/'
    print(f'Checkpoints will be saved to: {checkpoint_dir}')
else:   log_dir = None

##################################################################################################################

# train model
print(f'Start training on dataset {data_dir} ...')
start_timestamp = time.time()
trainer(model=model,
        train_loader=train_loader,
        val_loader=val_loader,
        lr=lr,
        epochs=epochs,
        weight_decay=weight_decay,
        log_dir=log_dir,
        checkpoint_dir=checkpoint_dir,
        train_from_checkpoint=train_from_checkpoint_path,
        device=device)
time_passed = time.time()-start_timestamp
print(f'... training finished in {time_passed:.0f}s.')

##################################################################################################################

# save model
model_name = model_str+'.pth'
torch.save(model, save_path+model_name)
print('Model saved to: ' + save_path + model_name)
